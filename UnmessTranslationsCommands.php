<?php

namespace Drush\Commands\unmess_translations;

use Drush\Commands\DrushCommands;
use Drush\Drush;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

/**
 * Ensures the current language is in use.
 */
class UnmessTranslationsCommands extends DrushCommands {

  /**
   * Clear messed data.
   *
   * @hook post-command *
   */
  public function postCommand() {
    // Drupal must be fully bootstrapped in order to use this validation.
    $boot_manager = Drush::bootstrapManager();
    $boot_manager->bootstrapMax();
    if (!$boot_manager->hasBootstrapped(DRUSH_BOOTSTRAP_DRUPAL_FULL)) {
      $this->logger()->notice('unmess_translations: Nothing to do, no full boot.');
      return;
    }
    try {
      /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager */
      $entityFieldManager = \Drupal::service('entity_field.manager');
    } catch (ServiceNotFoundException $exception) {
      // If we do not have these services, this command is not really useful.
      $this->logger()->error($exception->getMessage());
      return;
    }
    $entityFieldManager->clearCachedFieldDefinitions();
    $this->logger()->success('unmess_translations did its cleanup.');
  }

}
